#include <iostream>
using namespace std;

int main(int argc, char** argv) {
	int i = 5 ;
	
	cout<<"i++\n";
	cout<<i<<"\n";
	cout<<i++<<"\n";
	cout<<i<<"\n";
	cout<<"--------\n\n";
	
	i = 5;
	
	
	cout<<"++i\n";
	cout<<i<<"\n";
	cout<<++i<<"\n";
	cout<<i<<"\n";
	cout<<"--------\n\n";
	
	cout<<"i--\n";
	cout<<i<<"\n";
	cout<<i--<<"\n";
	cout<<i<<"\n";
	cout<<"--------\n\n";
	
	cout<<"--i\n";
	cout<<i<<"\n";
	cout<<--i<<"\n";
	cout<<i<<"\n";
	cout<<"--------\n\n";
	
	return 0;
}
