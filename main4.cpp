#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;
int main(int argc, char** argv) {
	
	float x,y;
	cout<<"--------CALCULO DE POLINOMIO--------\n";
	cout<<"P(x) = 2x^3 + 7x^2 + 3x - (5/2)\n";
	cout<<"Entre com o valor de X: ";
	cin>>x;
	
	y = 2*(x*x*x) + 7 *(x*x) + 3 * x - (5/2);
	cout<< "Resultado de P(x) e: "<<y<< "\n";
	
	return 0;
}
