#include <iostream>
using namespace std;

int main(int argc, char** argv) {

	int num,resposta;
	int acertos;
	int erros;
	acertos = 0;
	erros = 0;
	
	
	cout<<"Digite a tabuada que voc� quer estudar: ";
	cin>>num;
	
	
	for(int i=1;i<=10;i++){
		cout<<num<<" x "<<i<<" = ";
		cin>>resposta;
		
		if(resposta == (num*i)){
			cout<<"acertou ;-)\n";
			acertos++;
		}
		else{
			cout<<" errou ;-(\n";
			erros++;
		}
	}
	
	if(acertos == 10){
		cout<< "PARABENS! voce acertou tudo!\n";
	}
	
	else{
		cout<<"voce teve "<<acertos<<" acertos e " <<erros<< " erros\n";
	}

	return 0;
}
