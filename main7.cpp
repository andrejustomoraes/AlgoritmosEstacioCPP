#include <iostream>
#include <cmath>
using namespace std;

int main(int argc, char** argv) {
	
	float peso,altura,imc;
	
	cout<<"digite seu peso: ";
	cin>>peso;
	cout<<"digite a altura: ";
	cin>>altura;
	
	imc = peso/(pow(altura,2));
	
	if(imc<17){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Muito abaixo do peso"<<endl;
	}
	if(17<=imc && imc<18.49){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Abaixo do peso"<<endl;
	}
	if(18.5<imc && imc<24.99){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Peso normal"<<endl;
	}
	if(25<imc && imc<29.99){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Acima do peso"<<endl;
	}
	if(30<imc && imc<34.99){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Obesidade I"<<endl;
	}
	if(35<imc && imc <39.99){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Obesidade II (severa)"<<endl;
	}
	if(40<imc){
		cout<<"\n\nIMC= "<<imc<<".Situacao: Obesidade III (morbida)"<<endl;
	}
	
	return 0;
}
